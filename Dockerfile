# Fetch latest Alpine Linux Docker image
# and add it as a layer. 
FROM alpine:latest

# Set Hugo version.
ENV HUGO_VERSION 0.92.1
ENV HUGO_RESOURCE hugo_extended_${HUGO_VERSION}_Linux-64bit.tar.gz

# Install requirements for Hugo.
RUN apk add --update \
      openssh \
      git  \
      libc6-compat \
      libstdc++ \
      musl-dev \
      go

# Delete cache in order to save some space.
RUN rm -rf /var/cache/apk/*

# Configure Go.
ENV GOROOT /usr/lib/go
ENV GOPATH /go
ENV PATH /go/bin:$PATH

# Install Hugo and remove temporary files to save some space.
ADD https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_RESOURCE} /tmp/

RUN mkdir /tmp/hugo && \
    tar -xvzf /tmp/${HUGO_RESOURCE} -C /tmp/hugo/ && \
    mv /tmp/hugo/hugo /usr/bin/hugo && \ 
    rm -rf /tmp/hugo*
